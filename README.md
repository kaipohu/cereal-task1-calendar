# Cereal Task1 Calendar

### ReadMe  
在.env中可以選設定參數  
**REACT_APP_LOCALE**: 語系，支援en和zh-tw  
**REACT_APP_IS_ISOWEEK**: 1: 每週第一天為週一 / 0: 每週第一天為週日  
  
### 瀏覽器  
Chrome 79.0.3945.79 (通過)  
Microsoft Edge 42.17134.1038.0 (通過)  
