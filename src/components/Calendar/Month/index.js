import React, { useCallback } from 'react';
import classnames from 'classnames';
import moment from 'moment-timezone';

import classes from "../style/style.module.scss";

const is_isoWeek = process.env.REACT_APP_IS_ISOWEEK;
const week = is_isoWeek === "1" ? "isoWeek" : "week";
const weekdaysIndexes = is_isoWeek === "1" ? [1, 2, 3, 4, 5, 6, 0] : [0, 1, 2, 3, 4, 5, 6];
const today = moment();

const Month = ({ month, year }) => {
    const weekdays = weekdaysIndexes.map((wd) => <div key={`weekday_${wd}`} className={classes["day"]} >{moment.weekdaysMin(wd)}</div>);

    const renderDays = useCallback(() => {
        const firstDayOfMonth = moment.utc( `${year}${month}01`, "YYYYMMDD").startOf("month");
        const firstDayOfFirstWeekOfMonth = firstDayOfMonth.clone().startOf(week);
        const lastDayOfMonth = firstDayOfMonth.clone().endOf("month");
        const lastDayOfLastWeekOfMonth = lastDayOfMonth.clone().endOf(week);

        const components = [(<div key={`month_row_weekday`} className={classnames(classes["month-week"])}>{weekdays}</div>)];

        let day = firstDayOfFirstWeekOfMonth;
        let row = [];

        while (day < lastDayOfLastWeekOfMonth) {
            const md = moment(day);
            const d = md.format("DD");
            const isDisabled = day < firstDayOfMonth || day > lastDayOfMonth;

            row.push(
                <div key={`date${year}${month}${d}`} className={classnames(classes.day, isDisabled ? classes.disabled : today.isSame(md, "date") ? classes.today : null)}>
                    <span>{d}</span>
                </div>
            );
            if (row.length === 7) {
                components.push((<div key={`month_row_${day.format("YYYYMMDD")}`} className={classes["month-week"]}>{row}</div>));
                row = [];
            }    
            day = day.clone().add(1, 'd');
        }

        return components;
    }, [month, year, weekdays]);

    return (
        <div className={classes.month}>
            <span className={classes["month-title"]}>{moment.months(parseInt(month)-1)}</span>
            {renderDays()}
        </div>
    );
}

export default Month;