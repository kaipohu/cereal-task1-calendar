import React, { useState, useCallback, Fragment } from 'react';
import moment from 'moment-timezone';

import Month from './Month';

import classes from "./style/style.module.scss";

const Calendar = () => {
    const today = moment();

    const [year, setYear] = useState(parseInt(today.format("YYYY")));
    const [inputValue, setInputValue] = useState(parseInt(today.format("YYYY")));
    const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    const submitNotAllow = useCallback(() => {
        const value = parseInt(inputValue);
        return isNaN(inputValue) || isNaN(value) || (value < 1 || value > 9999);
    }, [inputValue]);

    return (
        <div className={classes.calendar}>
            <div className={classes["calendar-title"]}>
                <form onSubmit={(e) => {
                    e.preventDefault();
                    setYear(parseInt(inputValue));
                }}>
                    <input type="text" name="year" className={classes["year-input"]} value={inputValue} onChange={(e) => {
                        setInputValue(e.target.value);
                    }}/>
                    {
                        submitNotAllow() ? (
                            <Fragment>
                                <button className={classes["submit-button"]} type="button" disabled={true}>SHOW</button>
                                <div className={classes.error}>Please enter a valid value. The number you enter must be between 1 and 9999, including 1 and 9999. <span onClick={() => { setInputValue(year); }}>redo</span></div>
                            </Fragment>
                        ) : (
                            <button className={classes["submit-button"]} type="submit">SHOW</button>
                        )
                    }
                </form>
            </div>
            {
                !isNaN(year) && (year >= 1 || year <= 9999) && (
                    <div>
                        {months.map(month => <Month key={`${year}_${month}_m`} month={('0' + month).slice(-2)} year={('000' + year).slice(-4)} />)}
                    </div>
                )
            }
        </div>
    );
}

export default Calendar;