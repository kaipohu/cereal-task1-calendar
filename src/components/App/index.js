import React from 'react';
import moment from 'moment-timezone';
import "moment/locale/zh-tw";

import Calendar from 'components/Calendar';

//is_isoWeek => monday to sunday, otherwise sunday to saturday
const locale = process.env.REACT_APP_LOCALE;
const is_isoWeek = process.env.REACT_APP_IS_ISOWEEK;

moment.locale(locale, {
    week: {
        dow: parseInt(is_isoWeek),
    },
});

const App = () => {
    return (
        <Calendar />
    );
}

export default App;